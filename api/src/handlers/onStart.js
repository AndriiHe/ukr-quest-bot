const sendQuestFinished = require('../utils/sendQuestFinished');
const { mergeMap } = require('rxjs/operators');
const { from, of } = require('rxjs');
const { IMAGE, AUDIO, VIDEO, FILE } = require('./attachmentType');

module.exports = (container) => ({ chat }) => {
  const { bot, userService, questService, questions } = container;
  const user = { chatId: chat.id, firstName: chat.first_name, lastName: chat.last_name };

  const createUser = () => userService.create(user);
  const handleNextMessage = (quest) => {
    const nextQuestion = questions[quest.currentQuestion - 1];
    switch(nextQuestion.attachment_type) {
      case IMAGE:
        return bot.sendPhoto(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      case AUDIO:
        return bot.sendAudio(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      case VIDEO:
        return bot.sendVideo(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      case FILE:
        return bot.sendAnimation(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      default:
        return bot.sendMessage(chat.id, nextQuestion.text, { disable_web_page_preview: true });
    }
  };
  const sendNextQuestion = (quest) => from(handleNextMessage(quest));

  userService.isExist(user.chatId).pipe(
    mergeMap((isExist) => isExist ? of(isExist) : createUser()),
    mergeMap(() => questService.getQuestByChatId(chat.id)),
    mergeMap((quest) => quest.finishedAt && !quest.currentQuestion ? sendQuestFinished(chat.id, bot) : sendNextQuestion(quest)),
  ).subscribe(() => console.log(`User ${user.chatId} (${user.firstName} ${user.lastName}) trigger START action`));
};
