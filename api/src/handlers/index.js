const { MESSAGE, START } = require('../actions');
const onMessage = require('./onMessage');
const onStart = require('./onStart');

module.exports = (container) => {
  const { bot } = container;
  bot.onText(MESSAGE, onMessage(container));
  bot.onText(START, onStart(container));
  bot.on("polling_error", (err) => console.log(err));
};
