const sendQuestFinished = require('../utils/sendQuestFinished');
const { mergeMap, map  } = require('rxjs/operators');
const { IMAGE, AUDIO, VIDEO, FILE } = require('./attachmentType');
const finalMessage = require('./finalMessage');
const { from, of } = require('rxjs');

module.exports = (container) => ({ chat, text }) => {
  const { questService, bot, responseService, questions } = container;

  const finishQuest = () => from(bot.sendMessage(chat.id, finalMessage));
  const handleNextMessage = (quest) => {
    const nextQuestion = questions[quest.currentQuestion];
    switch(nextQuestion.attachment_type) {
      case IMAGE:
        return bot.sendPhoto(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      case AUDIO:
        return bot.sendAudio(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      case VIDEO:
        return bot.sendVideo(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      case FILE:
        return bot.sendDocument(chat.id, nextQuestion.attachment_url, { caption: nextQuestion.text });
      default:
        return bot.sendMessage(chat.id, nextQuestion.text, { disable_web_page_preview: true });
    }
  };
  const sendNextQuestion = (quest) => from(handleNextMessage(quest));

  const successResponse = (quest, question) => {
    const hasNextQuestion = !!questions[quest.currentQuestion];
    return responseService.applyResponse({
      questId: quest.id,
      questionId: question.id,
      text,
      isCorrect: true,
      nextQuestion: hasNextQuestion ? quest.currentQuestion + 1 : null ,
      finishedAt: !hasNextQuestion ? new Date() : null
    }).pipe(
      mergeMap(() => hasNextQuestion ? sendNextQuestion(quest) : finishQuest())
    )
  };

  const rejectResponse = (quest, question) => responseService.rejectResponse({ questId: quest.id, questionId: question.id, text, isCorrect: false }).pipe(
    map(() => bot.sendMessage(chat.id, "Не вірно. Спробуйте ще раз!"))
  );

  const applyResponse = (quest, response) => of(questions[quest.currentQuestion - 1]).pipe(
    map(question => !question.answers || !question.answers.length|| question.answers.some(answer => answer.toLowerCase() === response.toLowerCase())),
    mergeMap(isCorrect => isCorrect ? successResponse(quest, questions[quest.currentQuestion - 1]) : rejectResponse(quest, questions[quest.currentQuestion - 1]))
  );

  questService.getQuestByChatId(chat.id).pipe(
    mergeMap((quest) => quest.finishedAt && !quest.currentQuestion ? sendQuestFinished(chat.id, bot) : applyResponse(quest, text)),
  ).subscribe(() => console.log(`User ${chat.id} (${chat.first_name} ${chat.last_name}) sent "${text}"`));
};
