const UserService = require('./userService');
const QuestService = require('./questService');
const QuestionService = require('./questionService');
const ResponseService = require('./responseService');

module.exports = {
  ResponseService,
  UserService,
  QuestService,
  QuestionService,
};
