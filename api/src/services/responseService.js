const { map } = require('rxjs/operators');

module.exports = class {
  constructor(ioc) {
    this.db = ioc.db;
  }

  applyResponse({ questId, nextQuestion, finishedAt, questionId, text, isCorrect  }) {
    return this.db.transaction(
      (db) => db.query(`UPDATE quests SET current_question=$2, finished_at=$3 WHERE id=$1`, [questId, nextQuestion, finishedAt]),
      (db) => db.query(`INSERT INTO responses (quest_id, question_id, text, is_valid) VALUES ($1, $2, $3, $4)`, [questId, questionId, text, isCorrect])
    );
  }

  rejectResponse({ questId, questionId, text, isCorrect }) {
    return this.db.query(`INSERT INTO responses (quest_id, question_id, text, is_valid) VALUES ($1, $2, $3, $4)`, [questId, questionId, text, isCorrect])
  }
};
