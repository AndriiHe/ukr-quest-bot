const { map } = require('rxjs/operators');

module.exports = class {
  constructor({ db }) {
    this.db = db;
  }

  getQuestByChatId(chatId) {
    return this.db.query(`SELECT q.* FROM quests q LEFT JOIN users u ON q.user_id = u.id WHERE u.chat_id=$1`, [chatId]).pipe(
      map(({ rows: [quest] }) => ({
        id: quest.id,
        userId: quest.user_id,
        currentQuestion: quest.current_question,
        startedAt: quest.started_at,
        finishedAt: quest.finished_at,
      }))
    );
  }
};
