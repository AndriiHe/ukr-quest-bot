const { map } = require('rxjs/operators');

module.exports = class {
  constructor(ioc) {
    this.db = ioc.db;
  }

  getQuestions() {
    return this.db.query(`SELECT row_to_json(row) FROM ( SELECT id, text, attachment_type, attachment_url, (SELECT array_to_json(array_agg(answers.text)) FROM (SELECT text, attachment_type, attachment_url FROM answers a WHERE a.question_id=q.id ORDER BY a.id ) answers ) AS answers FROM questions q ORDER BY id) row;`).pipe(
      map(({ rows }) => rows.map(row => row.row_to_json))
    );
  }
};
