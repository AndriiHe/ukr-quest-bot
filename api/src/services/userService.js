const { map } = require('rxjs/operators');

module.exports = class {
  constructor({ db }) {
    this.db = db;
  }

  create(user) {
    return this.db.transaction(
      (db) => db.query(`INSERT INTO users(chat_id, first_name, last_name) VALUES($1, $2, $3) RETURNING id;`, [user.chatId, user.firstName, user.lastName]),
      (db, { rows: [ user ] }) => db.query('INSERT INTO quests(user_id) VALUES ($1);', [user.id]),
    ).pipe(
      map(({ rows }) => rows[0])
    );
  };

  isExist(chat_id) {
    return this.db.query(`SELECT * FROM users WHERE chat_id=$1`, [chat_id]).pipe(
      map(({ rowCount }) => rowCount > 0)
    );
  }
};
