const db = require('./db');
const { UserService, QuestService, QuestionService, ResponseService } = require('./services');
const TelegramBot = require('node-telegram-bot-api');

const token = process.env.TELEGRAM_BOT_TOKEN || '';

module.exports = class {
  getInstance() {
    if (!this.ioc) {
      this.ioc = {};
      this.ioc.db = db;
      this.ioc.bot = new TelegramBot(token, { polling: true });
      this.ioc.userService = new UserService(this.ioc);
      this.ioc.questService = new QuestService(this.ioc);
      this.ioc.questionService = new QuestionService(this.ioc);
      this.ioc.responseService = new ResponseService(this.ioc);
      this.ioc.questionService.getQuestions().subscribe(questions => this.ioc.questions = questions);
    }

    return this.ioc;
  }
};
