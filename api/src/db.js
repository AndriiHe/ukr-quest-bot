const { RxPg } = require('rxpg');

module.exports = new RxPg(process.env.DATABASE_URL || 'postgres://admin:secret@localhost:5432/ukr_quest_db');
