const IocContainer = require('./ioc');
const mountHandlers = require('./handlers');

const container = new IocContainer().getInstance();

mountHandlers(container);
