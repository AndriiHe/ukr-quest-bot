create TABLE IF NOT EXISTS users
(
    id SERIAL PRIMARY KEY,
    chat_id VARCHAR(100) NOT NULL,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    role INT DEFAULT 1 REFERENCES roles(id) NOT NULL
);

insert into migrations (name) values ('20200713204800__create_users_table');
