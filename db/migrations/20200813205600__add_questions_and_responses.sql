INSERT INTO questions (text, attachment_type, attachment_url)
VALUES (E'Привіт! Ось тут невеличка інструкція до квесту\n\nЯк називається твоя команда?', 1, 'https://drive.google.com/uc?id=1AdjzkeBO0jZjq7orvXLEDFW5ucfR94Ds '),
       (E'Невеличке завдання для розминки. Перевіримо твої знання про Юкрейнер. В кінці на тебе чекатиме кодове слово, яке і буде ключем до наступного завдання.\nhttps://bit.ly/3k2y0dm', NULL, NULL),
       (E'Молодці! Гарна робота, але це була лише розминка. Далі — складніше.\n\nНаступне завдання потребуватиме від тебе знань “внутряка” Юкрейнера. З десяти фраз тобі потрібно обрати ті п’ять, які відомі в межах проєкту, своєрідні мемчики. Кожна з фраз буде вести тебе на нову сторінку, але лише з правильно обраних варіантів ти зможеш отримати ключ до наступного завдання.', 1, 'https://drive.google.com/uc?id=1oOV7EU3_oun5t05NgSXw21Oe9FYFAgK1'),
       (E'Круто! Це було непросто, але ви впоралися! Маланка — це гра з формами, поєднання непоєднуваного і справжній сюрреалізм. Тож давайте зануримося в атмосферу Маланки і перевіримо вашу креативність.\nНе бійтеся виглядати безглуздо — хіба не таке кредо у маланкарів? Візьміть його собі на озброєння.\n\nЯ зараз надішлю ребус з картинок. Не бійтеся відкрити наші історії про маланку й пошукати, як називаються певні явища або місця. Складіть цей ребус і надішліть мені розгадку.', 1, 'https://drive.google.com/uc?id=1itfGQ28CVUxBcRKGp3Vx4qo_euHx1369'),
       (E'З картинками впоралися добре, а як щодо сприйняття української мови та її діалектів на слух?\n\nРозшифруйте те, що почуєте, і надішліть @lerkindidenko це текстом. Вона перевірить і лиш якщо у вас там не буде помилок (ну або майже не буде), отримаєте кодове слово. Скажіть мені його', 3, 'https://drive.google.com/uc?id=1E0hrrvbcUrYeTG7QwKMrrui-hSM69ZmA'),
       (E'Я бачу, ви розумієтеся на тонкощах української самобутності. Адже діалекти і є душею мови.\nДавайте перевіримо, чи добре ви знаєте історію України.\n\nАналогічно до першого завдання, чекаю від вас одне слово — ключ до наступного завдання.\nhttps://bit.ly/2BOkwAA ', NULL, NULL),
       (E'О, вочевидь ви дружите з історією України. І правильно. Давайте перейдемо від теорії до практики.\n\nПерейдіть за лінком, зареєструйтеся на сайті.\nhttps://bit.ly/3ftMZJY\nУ вас буде п’ять завдань, в яких ви маєте впізнати місцевість і якнайточніше позначити її на мапі України. Мапа буде у правому нижньому кутку екрана.\n\nНадішліть скріншот з кількістю балів @lerkindidenko. Якщо ви наберете більше 10 000 балів, отримаєте ключ до наступного завдання. Скажіть мені його.', NULL, NULL),
       (E'Після того, як трішки відчули себе експедитором, використайте вже здобутий досвід подорожей та сплануйте поїздку Україною. Нам потрібно доїхати до певного місця, але ми забули, як воно називається.\n\nВтім, наші друзі нам допоможуть. Спершу треба заїхати до Майкла Щура, забрати його, потім Віктора Павліка, Валерія Харчишина, Альону Альону. З ними всіма станцювати Танок На Майдані Конго, заскочити по Яніну Соколову та врешті — по Brunettes Shoot Blondes. Попросіть їх покласти маршрут та нагадати вам назву місця', 1, 'https://drive.google.com/uc?id=1eaxar6FqzZ_LW-iT5AdHG6vpCSKFS7v3'),
       (E'Ого! Я бачу, складні завдання вам по силах. Приємно вражений. А як щодо такого?\n\nЯ згадав круті фрази наших героїв, але пропустив одне слово. Вгадаєш яке? Напиши його за допомогою emoji.\n\n“Що ж робити? Немає ______. Ми загинемо. Людство загине”.\n\n“_____, вона розумна тварина. Люди думають, що вони дурні — нє, ніфіга. Характер у них тільки важкий. ______” (тут одне emoji)\n\n“Карпатський буйвол — це український _____”.', NULL, NULL),
       (E'Нічого собі! Слухай, я вже трішки голодний і хочу перекусити. У нас є матеріали, з основних тем яких можна зготувати вечерю. Напиши мені чотири emoji, які відповідатимуть цій послідовності:\n\nВК Д У Г', NULL, NULL),
       (E'Круто! Так, кожна історія Ukraїner унікальна. Але чи запам’ятовуєте ви, в чому саме вона полягає? Зараз будуть кілька питань, які перевірять вашу уважність.\n\nГеллі Бері, Елвіс Преслі, Мерлін Монро — одного виду. А Наомі Кемпбел, Тайра Бенкс та Лінда Євангеліста — іншого. І всі вони живуть під одним дахом. Де?', NULL, NULL),
       (E'Яке єдине відео в Юкрейнері має обмеження 18+?', NULL, NULL),
       (E'Що об’єднує радіацію, мотобол, музей, витинанки, маяк, ліжники і бортників?', NULL, NULL),
       (E'Впізнаєте з якого регіону жінка, яка співає це?', 2, 'https://drive.google.com/uc?id=1XnudXy-0FWAZFULjuIn-n7yk9g_QqvUL'),
       (E'А тут здогадаєтеся про кого йдеться в цій гіфці?', 4, 'https://drive.google.com/uc?id=1TnTzbTd9t-5q2Q8na-KYWMXHkP4QZiUz'),
       (E'Зв’язковий із Закарпаття допоможе розшифрувати це:\n\nLewlkpapvu', NULL, NULL),
       (E'Ну і наостанок, зазирніть сюди!\n\nСеред багатьох точок зору оберіть ту, яка допоможе найкраще розгледіти місцевість.', 1, 'https://drive.google.com/uc?id=1MRshq1qPNjCV_poyqNTW8RvRD3yzzmGW');

INSERT INTO answers(question_id, text)
VALUES (2, 'Єнот'),
       (3, 'Маланка'),
       (4, 'Навала туристів'),
       (5, 'Ніфіга-ніфіга'),
       (5, 'ніфіга ніфіга'),
       (6, 'Маршрут'),
       (7, 'Амбасадори'),
       (8, 'Домажир'),
       (9, '🐝🐷🐘'),
       (9, '🐝🐽🐘'),
       (10, '🥬🥒🍅🥔'),
       (11, 'Вербівка'),
       (11, 'Вербівкка'),
       (11, 'Вербовка'),
       (12, 'Харків від ТНМК'),
       (12, 'ТНМК'),
       (12, 'танок на майдані конго'),
       (12, 'танок на майдані'),
       (13, 'Ukraїner. The Movie'),
       (13, 'Ukrainer. The Movie'),
       (13, 'ukrainer.the movie'),
       (13, 'Юкрейнер. Зе муві'),
       (13, 'юкрейнер зе муві'),
       (13, 'Зе муві'),
       (13, 'The Movie'),
       (14, 'Полісся'),
       (14, 'поліся'),
       (14, 'співи полісся'),
       (14, 'співи поліся'),
       (15, 'Грузини України'),
       (15, 'Грузини'),
       (16, 'Expedition'),
       (16, 'експедиція'),
       (17, '11'),
       (17, 'одинадцять');
