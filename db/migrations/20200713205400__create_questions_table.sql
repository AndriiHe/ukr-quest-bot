CREATE TABLE IF NOT EXISTS questions
(
    id SERIAL PRIMARY KEY,
    text VARCHAR NOT NULL,
    attachment_type VARCHAR,
    attachment_url VARCHAR
);

INSERT INTO migrations (name) VALUES ('20200713205400__create_questions_table');
