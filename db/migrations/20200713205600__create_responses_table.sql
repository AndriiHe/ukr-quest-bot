CREATE TABLE IF NOT EXISTS responses
(
    id SERIAL PRIMARY KEY,
    quest_id INT REFERENCES quests(id) NOT NULL,
    question_id INT REFERENCES questions(id) NOT NULL,
    text VARCHAR NOT NULL,
    is_valid BOOLEAN
);

INSERT INTO migrations (name) VALUES ('20200713205600__create_responses_table');
