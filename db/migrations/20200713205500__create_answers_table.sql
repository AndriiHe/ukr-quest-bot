CREATE TABLE IF NOT EXISTS answers
(
    id SERIAL PRIMARY KEY,
    question_id INT REFERENCES questions(id) NOT NULL,
    text VARCHAR(500) NOT NULL
);

INSERT INTO migrations (name) VALUES ('20200713205500__create_answers_table');
