CREATE TABLE IF NOT EXISTS roles
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

INSERT INTO roles (name) VALUES ('user'), ('admin');

INSERT INTO migrations (name) VALUES ('20200713204700__create_roles_table');
