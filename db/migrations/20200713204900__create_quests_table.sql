CREATE TABLE IF NOT EXISTS quests
(
    id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users(id) NOT NULL,
    current_question INT DEFAULT 1 NULL,
    started_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    finished_at TIMESTAMP NULL
);

INSERT INTO migrations (name) VALUES ('20200713204900__create_quests_table');
